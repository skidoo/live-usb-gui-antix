# live-usb-gui-antix

This utility shipped in antiX16, but in next antiX release it will probably be deprecated in favor of the more-richly-featured "live-usb-maker" utility:https://github.com/BitJam/live-usb-maker
as seen in MX16 release.

Confusingly (and difficult to maintain), the underlying shell script "/usr/local/bin/antix2usb.sh" resided in a separate github repository
https://github.com/antiX-Linux/live-usb-cli-antix/blob/master/antix2usb.sh
the online version of which is older (as of this writing) than version actually shipped in the antiX16 release.

TODO:
-- add the .sh script into THIS repo
-- add a locally-installed helpfile (as is, a no-op Help button attempts to launch non-existent "mx-viewer"... which leads to a non-existent "mepis.org" webpage, then redirects to MX usermanual containing 4 not-very-helpful sentences regarding antix2usb)
